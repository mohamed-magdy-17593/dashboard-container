import {toast} from 'react-toastify'
import {css} from 'react-emotion'
import {green, blue, white, red} from '../../styles/js/colors'

const createToustEmiter = config => message =>
  console.log(config, message) || toast(message, config(message))

export const successToast = createToustEmiter(() => ({
  className: css({
    color: white,
    background: green,
    padding: '30px',
  }),
}))

export const infoToast = createToustEmiter(() => ({
  className: css({
    color: white,
    background: blue,
    padding: '30px',
  }),
}))

export const dangerToast = createToustEmiter(() => ({
  className: css({
    color: white,
    background: red,
    padding: '30px',
  }),
}))
