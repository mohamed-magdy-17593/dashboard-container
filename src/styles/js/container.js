import React from 'react'
import {css} from 'emotion'
import {white, primary, dark, gray, whiteDark} from './colors'

export const center = css`
  display: flex;
  justify-content: center;
  align-items: center;
`

export const primaryBg = css`
  background-color: ${primary};
  color: ${white};
`

export const darkBg = css`
  background-color: ${dark};
  color: ${white};
`

export const grayBg = css`
  background-color: ${gray};
  color: ${dark};
`

export const whiteDarkBg = css`
  background-color: ${whiteDark};
  color: ${dark};
`
export const whiteBg = css`
  background-color: ${white};
  color: ${dark};
`

export function Row({className = '', ...props}) {
  return (
    <div
      className={`${className} row ${css`
        margin: 0;
        padding: 0;
        & > div {
          margin: 0;
          padding: 0;
        }
      `}`}
      {...props}
    />
  )
}
