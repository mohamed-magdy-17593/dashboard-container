import Color from 'color'

// export const primary = '#D88A8A'
// export const secondary = '#C5CFC6'
// export const primary = '#4DAB8C'
// export const secondary = '#C9306B'

export const green = '#00b894'
export const blue = '#0984e3'
export const red = '#e17055'
export const orange = '#fdcb6e'
export const yellow = '#ffeaa7'

export const primary = orange
export const secondary = yellow
export const dark = '#2d3436'
export const gray = '#b2bec3'
export const white = '#fff'

export const whiteDark = Color(white)
  .darken(0.03)
  .hex()

export const primaryDark = Color(primary)
  .darken(0.05)
  .hex()
