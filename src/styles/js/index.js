import {injectGlobal} from 'emotion'
import {dark} from './colors'

injectGlobal`
  @import url('https://fonts.googleapis.com/css?family=Open+Sans:400,700');
  body {
    font-family: 'Open Sans', sans-serif;
    color: ${dark};
  }
`
