import {css} from 'react-emotion'

export const cleanLink = css`
  text-decoration: none;
  color: inherit;
  &:hover {
    color: inherit;
    text-decoration: none;
  }
`
