import React from 'react'
import {Router} from '@reach/router'
import {ToastContainer} from 'react-toastify'
import Home from './components/Home'
import Login from './components/Login/Login'
import DashboardContainer from './components/Dashboard/DashboardContainer'
import DashboardHome from './components/Dashboard/Home'
import Users from './components/Dashboard/Users'
import Posts from './components/Dashboard/Posts'
import Subjects from './components/Dashboard/Subjects'

function App() {
  return (
    <>
      <ToastContainer
        hideProgressBar
        draggable
        pauseOnHover
        pauseOnVisibilityChange
      />
      <Router>
        <Home path="/" />
        <Login path="/login" />
        <DashboardContainer path="/dashboard">
          <DashboardHome path="/" />
          <Users path="users" />
          <Posts path="posts" />
          <Subjects path="subjects" />
        </DashboardContainer>
      </Router>
    </>
  )
}

export default App
