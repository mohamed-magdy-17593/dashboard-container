import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import 'react-toastify/dist/ReactToastify.css'
import './styles/index.sass'
import './styles/js'

ReactDOM.render(<App />, document.getElementById('root'))
