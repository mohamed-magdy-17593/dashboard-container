import React from 'react'
import {Card} from '../common/Card'
import {InputField} from '../common/InputField'
import {Formik, Form} from 'formik'
import {Button} from '../common/Button'
import {Row} from '../../styles/js/container'

function Home() {
  return (
    <div>
      <Row>
        <div className="col-12 col-md-8">
          <Card>
            <h1>Home</h1>
            <hr />
            <Formik>
              <Form>
                <InputField name="name" placeholder="name" />
                <InputField name="email" placeholder="email" />
                <InputField name="lola" placeholder="lola" />
                <InputField name="Yongane" placeholder="Yongane" />
                <Button>Submit</Button>
              </Form>
            </Formik>
          </Card>
        </div>
        <div className="col-12 col-md-4">
          <Card>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis
              expedita eaque accusamus maxime sapiente corporis dicta provident
              modi quibusdam officia similique autem, harum illo nihil. Ex
              praesentium odit facilis rerum.
            </p>
          </Card>
        </div>
      </Row>
      <Row>
        <div className="col-12">
          <Card>
            <h1>Some Text</h1>
            <hr />
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Consectetur, quas, eveniet distinctio dolorem voluptate sequi
              iste, quia sint beatae odit harum suscipit. Eos totam dolorum,
              cumque ipsa doloribus ex doloremque? Laboriosam quasi autem animi
              vero illum dolores nemo quos, dignissimos consectetur quisquam
              laudantium, facere suscipit sunt soluta sapiente adipisci! In,
              ipsum commodi. Accusamus cum placeat consequatur quisquam quo illo
              doloribus? Reprehenderit, maxime soluta doloremque maiores
              molestiae nisi accusamus voluptatum nesciunt dolore officiis
              inventore eligendi quasi est, porro dicta rerum dolorem
              perspiciatis? Consequatur dolores sint veritatis? Nam dicta aut
              architecto magnam? Soluta, harum laborum fugit ad corrupti
              incidunt perferendis voluptate esse quasi velit ex consequatur,
              non sit nulla! Officia quis maiores aspernatur esse, velit
              mollitia. Sapiente maxime sed neque odio illum? Ea, nobis ex
              repellat, animi eos quia enim in veritatis iste placeat
              repellendus! Libero doloremque, iusto tempore sapiente quod quis
              quidem in provident eum dolores impedit id, aliquid explicabo
              obcaecati. Vel officia nulla doloremque? Minima, voluptatum id
              optio vero ipsam, nostrum in veritatis tempora illum nihil sint et
              rerum quod sed illo, molestias cupiditate voluptas corrupti? Quod
              hic non tempora? Enim dolorum hic doloribus itaque numquam,
              repellat necessitatibus unde ipsa aperiam veniam molestiae at, ex
              eum, error architecto laudantium sint dolorem sit eaque voluptatem
              perspiciatis. Iste tempore ex ratione expedita? Dolor, quam ipsum!
              Voluptatibus sit optio mollitia quidem assumenda quos tempora,
              laborum asperiores cumque sapiente aliquid inventore! Error ab
              dolores repellendus asperiores eum? Veritatis ut beatae et vitae
              accusamus iste.
            </p>
          </Card>
        </div>
      </Row>
      <Row>
        <div className="col-12">
          <Card>
            <h1>Table</h1>
            <table className="table table-bordered">
              <thead>
                <tr>
                  <th>name</th>
                  <th>info</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>mohamed</td>
                  <td>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Autem ipsam quasi officiis alias perferendis repudiandae et
                    aperiam recusandae neque, corporis nulla ab assumenda cum
                    vitae ratione veritatis quo iste quos?
                  </td>
                </tr>
                <tr>
                  <td>Hoda</td>
                  <td>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Laborum reprehenderit molestiae autem deserunt corporis,
                    fuga odio maxime quo vitae quod placeat reiciendis,
                    perspiciatis labore non error, magni obcaecati eveniet
                    porro!
                  </td>
                </tr>
              </tbody>
            </table>
          </Card>
        </div>
      </Row>
    </div>
  )
}

export default Home
