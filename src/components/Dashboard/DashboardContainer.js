import React from 'react'
import styled, {css} from 'react-emotion'
import {Link} from '@reach/router'
import Fade from 'react-reveal/Fade'
import Flip from 'react-reveal/Flip'
import {primary, white, primaryDark} from '../../styles/js/colors'
import {cleanLink} from '../../styles/js/helpers'
import {NavLink} from '../common/NavLink'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {
  faUser,
  faNewspaper,
  faBook,
  faHome,
} from '@fortawesome/free-solid-svg-icons'
import {whiteDarkBg, whiteBg} from '../../styles/js/container'

const collapseWidth = 768

const DashboardLayOut = styled('div')`
  ${whiteDarkBg}
  display: grid;
  grid-template-columns: 1fr;
  @media (min-width: ${collapseWidth}px) {
    grid-template-columns: auto 1fr;
  }
`

const DashboardSideBar = styled('div')`
  color: ${white};
  background-color: ${primary};
  width: 100%;
  @media (min-width: ${collapseWidth}px) {
    grid-template-columns: auto 1fr;
    position: sticky;
    top: 0;
    width: 250px;
    height: 100vh;
  }
`

const DashboardMain = styled('div')`
  height: 3000px;
  ${whiteDarkBg}
`

const List = styled('div')`
  display: flex;
  min-width: 100%;
  width: 100%;
  overflow-x: auto;
  @media (min-width: ${collapseWidth}px) {
    flex-direction: column;
  }
`

const IconNavContainer = styled('span')`
  display: inline-block;
  width: 25px;
`

function ListItem({children, to, ...props}) {
  return (
    <NavLink
      className={`px-4 py-3 ${css`
        ${cleanLink}
        color: ${white};
        &:hover {
          background-color: ${primary};
        }
      `}`}
      to={to}
      activeStyle={{backgroundColor: primaryDark}}
      {...props}
    >
      {children}
    </NavLink>
  )
}

export const animationDuration = 350

function NavIcon({icon}) {
  return (
    <IconNavContainer>
      <FontAwesomeIcon icon={icon} />{' '}
    </IconNavContainer>
  )
}

function DashboardContainer({children}) {
  return (
    <DashboardLayOut>
      <Fade duration={animationDuration} left>
        <DashboardSideBar>
          <header className="px-4 py-4">
            <Link className={`${cleanLink}`} to="">
              Logo
            </Link>
          </header>
          <main>
            <List>
              <ListItem to="">
                <NavIcon icon={faHome} /> Home
              </ListItem>
              <ListItem to="users">
                <NavIcon icon={faUser} /> Users
              </ListItem>
              <ListItem to="posts">
                <NavIcon icon={faNewspaper} /> Posts
              </ListItem>
              <ListItem to="subjects">
                <NavIcon icon={faBook} /> Subjects
              </ListItem>
            </List>
          </main>
        </DashboardSideBar>
      </Fade>
      <DashboardMain>
        <Flip duration={animationDuration} bottom>
          <header className={`px-4 py-4 ${whiteBg}`}>Some header Info</header>
        </Flip>
        <main className={`p-3`}>{children}</main>
      </DashboardMain>
    </DashboardLayOut>
  )
}

export default DashboardContainer
