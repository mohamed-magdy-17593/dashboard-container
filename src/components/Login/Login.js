import React from 'react'
import styled from 'react-emotion'
import {navigate} from '@reach/router'
import * as yup from 'yup' // for everything
import {Formik, Form} from 'formik'
import Bounce from 'react-reveal/Bounce'
import {center, whiteDarkBg} from '../../styles/js/container'
import {InputField} from '../common/InputField'
import {Button} from '../common/Button'
import {successToast} from '../../services/toast'

const LoginContainer = styled('div')`
  width: 100%;
  height: 100vh;
  ${whiteDarkBg}
  ${center}
`

function Login() {
  return (
    <LoginContainer>
      <Bounce>
        <div className="col-12 col-sm-8 col-md-6 col-lg-4 bg-white p-4 rounded shadow">
          <h2>Login</h2>
          <hr />
          <Formik
            initialValues={{
              email: '',
              password: '',
            }}
            validationSchema={yup.object().shape({
              email: yup
                .string()
                .email()
                .required(),
              password: yup.string().required(),
            })}
            onSubmit={(values, {setSubmitting}) => {
              setTimeout(() => {
                alert(JSON.stringify(values, null, 2))
                setSubmitting(false)
                successToast('Welcome')
                navigate('/dashboard', {replace: true})
              }, 1000)
            }}
          >
            {({isSubmitting}) => (
              <Form>
                <InputField type="email" name="email" placeholder="Email" />
                <InputField
                  type="password"
                  name="password"
                  placeholder="Password"
                />
                <Button type="submit" loading={isSubmitting} block>
                  Login
                </Button>
              </Form>
            )}
          </Formik>
        </div>
      </Bounce>
    </LoginContainer>
  )
}

export default Login
