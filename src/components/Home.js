import React from 'react'
import {Link} from '@reach/router'

function Home() {
  return (
    <div
      style={{
        height: '100vh',
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <h1 className="font-weight-bold">
        Home{' '}
        <span role="img" aria-label="cool">
          💩
        </span>{' '}
        <Link to="login">login</Link>
      </h1>
    </div>
  )
}

export default Home
