import React from 'react'
import {css} from 'react-emotion'
import {white} from '../../styles/js/colors'
import Bounce from 'react-reveal/Bounce'

export function Card(props) {
  return (
    <Bounce>
      <div
        className={`p-4 m-3 rounded ${css`
          background: ${white};
        `}`}
        {...props}
      />
    </Bounce>
  )
}
