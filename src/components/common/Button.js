import React from 'react'
import {css} from 'react-emotion'
import Color from 'color'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faSpinner} from '@fortawesome/free-solid-svg-icons'
import {primary, white} from '../../styles/js/colors'

export function Button({
  bgColor = primary,
  textColor = white,
  loading,
  block,
  children,
  ...props
}) {
  return (
    <button
      className={`btn ${css`
        color: ${textColor};
        background-color: ${bgColor};
        &:hover {
          background-color: ${Color(bgColor)
            .darken(0.1)
            .hex()};
        }
      `} ${block ? 'btn-block' : ''}`}
      disabled={loading}
      {...props}
    >
      {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : children}
    </button>
  )
}
