import React from 'react'
import {css} from 'react-emotion'
import {Field, ErrorMessage} from 'formik'

export function InputField({label, name, id = name, placeholder, ...rest}) {
  return (
    <div className="form-group">
      {label ? (
        <label className="font-weight-bold" htmlFor={id}>
          {label}
        </label>
      ) : null}
      <Field
        className={`form-control ${css`
          border-width: 2px;
        `}`}
        placeholder={placeholder}
        name={name}
        id={id}
        {...rest}
      />
      <div className="text-danger">
        <small>
          <ErrorMessage name={name} />
        </small>
      </div>
    </div>
  )
}
