import React from 'react'
import {Link} from '@reach/router'

export function NavLink({activeStyle, ...props}) {
  return (
    <Link
      {...props}
      getProps={({isCurrent, style = {}}) => ({
        style: isCurrent ? activeStyle : style,
      })}
    />
  )
}
